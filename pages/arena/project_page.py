import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions

class ProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def search_for_project(self, search_term):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(search_term)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def verify_projects_found(self, search_term):
        found_projects = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
        assert len(found_projects) > 0

    def add_new_project(self, project_name, project_prefix):
        self.browser.find_element(By.CSS_SELECTOR, '.button_link').click()
        self.browser.find_element(By.CSS_SELECTOR, '.formContainer #name').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '.formContainer #prefix').send_keys(project_prefix)
        self.browser.find_element(By.CSS_SELECTOR, 'span #save').click()

        wait = WebDriverWait(self.browser, 10)
        message_area = (By.CSS_SELECTOR, '.j_close_button')
        wait.until(expected_conditions.element_to_be_clickable(message_area))
        self.browser.find_element(By.CSS_SELECTOR, '.j_close_button').click()

    def go_to_project_page(self):
        self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
