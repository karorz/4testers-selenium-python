from selenium.webdriver.common.by import By

logout_selector = '.icons-switch'
administartor_selector = '.icon_tools'

class HomePage:

    # Inicjalizacja klasy - przekazanie drivera/przeglądarki
    def __init__(self, browser):
        self.browser = browser

    # otwarcie strony
    def click_logout(self):

        self.browser.find_element(By.CSS_SELECTOR, logout_selector).click()

    def click_on_administrator_link(self):
        self.browser.find_element(By.CSS_SELECTOR, administartor_selector).click()

