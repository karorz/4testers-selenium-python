import string
import random
import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions

from pages.arena.home_page import HomePage
from pages.arena.login_page import LoginPage
from pages.arena.project_page import ProjectPage

administrator_email = 'administrator@testarena.pl'

@pytest.fixture
def browser():
    # Część która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    yield browser
    browser.quit()

def get_random_string(length):
 return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

# Login to TestArena page
def test_testarena_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email

# Go to Project page
def test_testarena_projects(browser):
    home_page = HomePage(browser)
    home_page.click_on_administrator_link()

    project_name = get_random_string(10)
    project_prefix = get_random_string(5)

    project_page = ProjectPage(browser)
    project_page.add_new_project(project_name, project_prefix)

    project_page.go_to_project_page()
    project_page.search_for_project(str(project_name))
    project_page.verify_projects_found(str(project_name))