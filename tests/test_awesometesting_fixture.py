import re
import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@pytest.fixture
def browser():
    # Część która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    browser.get('http://awesome-testing.blogspot.com/')

    # Coś co przekazujemy do kazdego testu
    yield browser

    # Część która wykona się po każdym teście
    browser.quit()

def test_post_count(browser):

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 4 elementy
    assert len(posts) == 4


def test_post_count_after_search(browser):

    # Inicjalizacja searchbara i przycisku search
    searchBar = browser.find_element(By.CSS_SELECTOR, '.gsc-input [title="search"]').send_keys('selenium')
    searchButton = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button').click()

    # Czekanie na stronę
    time.sleep(2)

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 20 elementów
    assert len(posts) == 20

def test_post_count_after_clicking_year_label(browser):

    # Inicjalizacja elementu z labelką 2019
    label = browser.find_element(By.LINK_TEXT, '2019').click()

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 10 elementów
    assert len(posts) == 10

def test_post_count_after_clicking_year_label_ver2(browser):
    years = browser.find_elements(By.CSS_SELECTOR, '#BlogArchive1_ArchiveList > ul')
    expected_number_of_posts = extract_the_number_of_posts_from_text(years[3].text)
    browser.find_element(By.LINK_TEXT, '2019').click()
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    assert len(posts) == expected_number_of_posts


def extract_the_number_of_posts_from_text(text):
    match = re.search(r'\((\d+)\)', text)
    return int(match.group(1))
