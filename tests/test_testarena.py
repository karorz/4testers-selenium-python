import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions

administrator_email = 'administrator@testarena.pl'

@pytest.fixture
def browser():
    # Część która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    browser.get('http://demo.testarena.pl/zaloguj')

    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(administrator_email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    # Coś co przekazujemy do kazdego testu
    yield browser

    # Część która wykona się po każdym teście
    browser.quit()

def test_testarena_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email

def test_testarena_logout(browser):
    browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()

    assert '/zaloguj' in browser.current_url
    assert browser.find_element(By.CSS_SELECTOR, '#password').is_displayed()

def test_testarena_open_messages(browser):
    browser.find_element(By.CSS_SELECTOR, '.top_messages').click()

    # Czekanie aż wydaży się jakaś akcja - poprawny sposób

    wait = WebDriverWait(browser, 10)
    message_area = (By.CSS_SELECTOR, '#j_msgContent')
    wait.until(expected_conditions.element_to_be_clickable(message_area))

    browser.find_element(By.CSS_SELECTOR, '#j_msgContent').send_keys('Wiadomość')

    # assert '/moje_wiadomosci' in browser.current_url

def test_testarena_projects(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys('Kamil')
    browser.find_element(By.CSS_SELECTOR, '.icon_search').click()

    found_projects = browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
    assert len(found_projects) > 0


