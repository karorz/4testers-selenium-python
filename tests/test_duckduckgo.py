import time

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com/')

    # Znalezienie paska wyszukiwania
    browser.find_element(By.CSS_SELECTOR, '#searchbox_input').send_keys('4_testers')

    # Znalezienie guzika wyszukiwania (lupki)
    browser.find_element(By.CSS_SELECTOR, '[aria-label=Search]').click()

    # Sprawdzenie że pierwszy wynik ma w sobie tytuł '4_testers - Kurs Tester Oprogramowania'
    time.sleep(2)
    titles = browser.find_elements(By.CSS_SELECTOR, '[data-testid=result-title-a] span')
    assert '4_testers - Kurs Tester Oprogramowania' in titles[0].text

    # Asercje że elementy są widoczne dla użytkownika

    # Szukanie Vistula University

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'

    # Zamknięcie przeglądarki
    time.sleep(5)
    browser.quit()
