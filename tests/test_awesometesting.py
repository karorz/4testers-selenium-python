import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_post_count():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')
    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 4 elementy
    assert len(posts) == 4
    # Zamknięcie przeglądarki
    time.sleep(2)
    browser.quit()

def test_post_count_after_search():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')

    # Inicjalizacja searchbara i przycisku search
    searchBar = browser.find_element(By.CSS_SELECTOR, '.gsc-input [title="search"]').send_keys('selenium')
    searchButton = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button').click()

    # Szukanie

    # Czekanie na stronę
    time.sleep(2)

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 20 elementów
    assert len(posts) == 20

    # Zamknięcie przeglądarki
    time.sleep(2)
    browser.quit()

def test_post_count_after_clicking_year_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    # Otwarcie strony
    browser.get('http://awesome-testing.blogspot.com/')

    # Inicjalizacja elementu z labelką 2019
    label = browser.find_element(By.LINK_TEXT, '2019').click()


    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 10 elementów
    assert len(posts) == 10

    # Zamknięcie przeglądarki
    time.sleep(2)
    browser.quit()